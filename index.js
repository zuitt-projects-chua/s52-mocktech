function countLetter(letter, sentence) {
    let result = 0;


    if(letter.length === 1){
        let match = new RegExp((letter), 'gi')
        let newmatch = sentence.match(match)
        if( newmatch.length > 0){
            return (newmatch.length)
        }else{
            return (newmatch)
        }
    }else{
            return undefined  
    }
    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

    
}


function isIsogram(text) {
   /* let newtext = text.toLowerCase()
    for (i = 0; i < text.length; i++){

        let match = new RegExp((text[i]), 'gi')
        let newmatch = text.match(match)
        if(newmatch > 1){
            return false
        }else{
            return true
        }
    }*/

    let newtext = text.toLowerCase()

    for (i = 0; i < text.length; i++){

        let match = new RegExp((text[i]), 'gi')
        let newmatch = text.match(match)

        if(newmatch.length > 1){
            //console.log(newmatch)
            return false
            
        }else{
            console.log(newmatch)
        }
    }

    return true

}

    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.



function purchase(age, price) {

    /*if(age < 13 ){
        return undefined
    }else {

        if(12 < age < 22){
            return (price*0.80)
        }else if (21 < age < 65){
            return (price)
        }
    }*/

    if(age < 13 ){
        return undefined  //(" test")
    }else if (age > 65) {
        return (price*0.80)
    }else if (age > 21){
        return (price.toFixed(2))
    }else if (age > 12){
        return (price*0.80)
    }else {
        return undefined
    }


    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    
}

function findHotCategories(items) {

    /*for (i = 0 ; i < items.length; i++){

        let nostocks = []

        if(items[i].stocks === 0){
            nostocks.push(items[i].category)
        }

        return nostocks

    }*/

    console.log(items + "test");
    const nostocks = [];

    for (i = 0 ; i < items.length; i++){
        console.log(items)

        if(items[i].stocks === 0){
            console.log("test")
            console.log(items[i].category)
            let category = items[i].category
            if(nostocks.includes(category)){
                console.log("not push")
            }else{
                nostocks.push(category) 
            }
        }
    }

    console.log(nostocks);  
    return (nostocks)




    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

}

function findFlyingVoters(candidateA, candidateB) {

    const voters = []

    for (i = 0; i < candidateA.length; i++){

        if(candidateB.includes(candidateA[i])){
            voters.push(candidateA[i])
        }

    }

    console.log(voters)
    return  voters


    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};